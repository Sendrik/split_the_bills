class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.belongs_to :visitor
      t.belongs_to :dinner

      t.integer :count
    end
  end
end
