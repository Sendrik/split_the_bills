class AddDoneStatusToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :done, :boolean
  end
end
