class MenuController < ApplicationController
  # before_action :authenticate_user!
  #session[:user_id]
  #
  def index
    #???
    @dinner = Dinner.new
    @dinners = Dinner.where(restaurant_id: "1")
  end

  def show
    @dinner = Dinner.find(params[:id])
  end

  def new
  end

  def create
    @dinner = Dinner.new(dinner_params)
    @dinner.save
    if(@dinner.save)
      redirect_to action: "index"
    else
      render 'new'
    end
  end

  private
  def dinner_params
    params.require(:dinner).permit(:name, :price)
  end

end
