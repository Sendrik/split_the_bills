$(document).ready(function(){

    let order_map = new Map();

    $MENU_ITEM = $('.ordered-menu-item');
    $ITEM_FOR_ORDER = $(".item-for-order");


    $(document).on( "click", '.add-order-item-button', function() {
        let dinner_id = $(this).attr("id");
        let dinner_name = $(this).closest(".menu-item").find(".menu-item-name").text();
        let dinner_price = $(this).closest(".menu-item").find(".menu-item-price").text();
        order_map.set(dinner_id, [(order_map.has(dinner_id)? order_map.get(dinner_id)[0]+1 : 1), dinner_name, dinner_price]);
        update_list();
    });

    $(document).on( "click", '.minus-order-item-button', function() {
        let dinner_id = $(this).attr("id");
        if (order_map.get(dinner_id)[0]<=1){
            order_map.delete(dinner_id);
        }
        else {
            order_map.set(dinner_id, [order_map.get(dinner_id)[0] - 1, order_map.get(dinner_id)[1], order_map.get(dinner_id)[2]]);
        }
        update_list();
        if(order_map.size==0){
            $('.make-order-button').hide();
        }
    });

    function update_list() {
        $('#visitor-order-list').empty();
        if (order_map.size>0){
            $('.make-order-button').show();
        }
        order_map.forEach(addElement);
    }

    function addElement(value, key, map) {
        $m_item = $MENU_ITEM.clone();
        $m_item.find('.menu-item-count').text(`${value[0]}x`);
        $m_item.find('.menu-item-name').text(value[1]);
        $m_item.find('.menu-item-price').text(value[2]*value[0]+" грн");
        $m_item.find('.minus-order-item-button').attr("id", key);
        $('#visitor-order-list').append($m_item.show());
    }


    $(document).on( "click", '.make-order-button', function() {
        let data = new Array();
        for (let key_item of order_map.keys()){
            data.push([key_item, order_map.get(key_item)[0]])
        }
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '/orders/addOrder',
            data: {order_list: data},
            success: function () {
                order_map.clear();
                $('#visitor-order-list').empty();
                $('.make-order-button').hide();
            },
            error: function () {
                alert("Упс\nСталась помилка, спробуйте ще раз");
            }
        });
    });

    $(document).on( "click", '.get-receipt', function() {
        $.ajax({
            type: "GET",
            url: '/orders/getReceipt',
            success: function (json) {
                console.log(json);
                let text = "";
                for (let j in json){
                    console.log(j);
                    text+="страва "+json[j]["dinner_id"]+" - х"+json[j]["count"]+"\n";
                }
                alert(text);
            },
            error: function () {
                alert("Упс\nСталась помилка, спробуйте ще раз");
            }
        });
    });

    $(document).on("click", '.confirm-order-menu-item', function () {
        let item_id = $(this).attr("id");
        console.log(item_id);
        let $this_item = $(this).closest(".order-item");
        $.ajax({
            type: "PATCH",
            url: '/table/confirmOrder',
            data: {order_id: item_id},
            success: function () {
                $this_item.remove();
            },
            error: function () {
                alert("Упс\nСталась помилка, спробуйте ще раз");
            }
        });
    });


    $(document).on("click", '.close-table-button', function () {
        let table_id = $(this).attr("id");
        $.ajax({
            type: "PATCH",
            url: '/table/closeTable',
            data: {table_id: table_id},
            success: function () {
                location.reload();
            },
            error: function () {
                alert("Упс\nСталась помилка, спробуйте ще раз");
            }
        });
    });

    $(document).on("click", ".delete-dinner", function () {
        let dinner_id = $(this).attr("id");
        let item = $(this).closest(".dinner-item");
        $.ajax({
            type: "DELETE",
            url: '/menus/deleteDinner',
            data: {dinner_id: dinner_id},
            success: function () {
                item.remove();
            },
            error: function () {
                alert("Упс\nСталась помилка, спробуйте ще раз");
            }
        });
    });
});