class Restaurant < ApplicationRecord
  has_many :dinners
  has_many :users
  has_many :tables
end
