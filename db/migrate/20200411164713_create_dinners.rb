class CreateDinners < ActiveRecord::Migration[6.0]
  def change
    create_table :dinners do |t|
      t.string :name
      t.float :price
      t.boolean :availability
      t.belongs_to :restaurant

      t.timestamps
    end
  end
end
