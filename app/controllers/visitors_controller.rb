class VisitorsController < ApplicationController

  def index
  end

  def new
  end

  def create
    table = Table.where(table_hash: params["table_hash"]).first
    new_visitor = Visitor.create(nickname: params["nickname"])

    bill = Bill.joins(:table)
        .where("table_id = ?", @table_id)
        .where("status = ?", "active")
        .first

    if bill.nil?
      bill = Bill.create(status: "active")
    end
    
    bill.visitors << new_visitor
    bill.save

    table.bills << bill
    table.save

    session[:table_id] = table.id
    session[:visitor_id] = new_visitor.id

    redirect_to "/orders"
  end

end
