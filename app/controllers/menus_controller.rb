class MenusController < ApplicationController
  before_action :authenticate_user!

  def index
    # todo
    @dinner = Dinner.new
    @dinners = Dinner.where(restaurant_id: "1")
  end

  def deleteDinner
    @dinner = Dinner.find(params[:dinner_id])
    @dinner.destroy
  end

  def show
    @dinner = Dinner.find(params[:id])
  end

  def new
  end

  def create
    @dinner = Dinner.new(dinner_params)
    @dinner.restaurant_id = "1"
    @dinner.availability = "1"
    if(@dinner.save)
      redirect_to action: "index"
    else
      render 'new'
    end
  end

  private
  def dinner_params
    params.require(:dinner).permit(:name, :price)
  end

end
