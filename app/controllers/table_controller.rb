class TableController < ApplicationController
  before_action :authenticate_user!
  #skip_before_action :verify_authenticity_token

  def index
    @tables = Table.where(restaurant_id: "1")
    @orders = Order.all
  end

  def show
    @tables = Table.where(restaurant_id: "1")
    @current_table = Table.find(params[:id])
    @orders = Order.joins(visitor: [{bill: :table}]).where("table_id=?", params[:id]).where("status=?", "active").where("done=?", "0")
  end

  def confirmOrder
    @id_param = params[:order_id]
    order = Order.find(@id_param)
    order.done = "1"
    order.save
  end

  def closeTable
    @id_param = params[:table_id]
    table = Bill.where("table_id=?", @id_param).where("status=?", "active").first
    table.status = "closed"
    table.save
  end

end
