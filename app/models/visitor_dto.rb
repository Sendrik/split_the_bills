class VisitorDto < ApplicationRecord
  def initialize(nickname: , table_name:)
    @nickname, @table_name = nickname, table_name
  end
end
