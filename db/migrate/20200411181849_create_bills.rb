class CreateBills < ActiveRecord::Migration[6.0]
  def change
    create_table :bills do |t|
      t.float :sum
      t.string :status
      t.belongs_to :user
      t.belongs_to :table

      t.timestamps
    end
  end
end
