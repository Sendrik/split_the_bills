class OrdersController < ApplicationController
  #protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  def index
    @menu = Dinner.where("availability=?", true)
    @visitor = Visitor.all.where(id: session[:visitor_id]).first
  end

  def addOrder
    @order_list = params[:order_list]
    @order_list.each { |order_item|
      @orders = Order.create(dinner_id: order_item[1][0], visitor_id: session[:visitor_id], count: order_item[1][1], done: "0")
    }
  end

  def getReceipt
    @dinner_list = Order.includes(:dinner).where("visitor_id=?", session[:visitor_id])
    #@dinner_list.each { |dinner_item|
    #  puts (dinner_item.name)
    #}
    #puts (@dinner_list)
    respond_to do |format|
      format.json {render json: @dinner_list}
    end
  end

end
