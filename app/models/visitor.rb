class Visitor < ApplicationRecord
  has_many :orders
  belongs_to :bill
end
