class AddHashToTables < ActiveRecord::Migration[6.0]
  def change
    add_column :tables, :table_hash, :string
  end
end
