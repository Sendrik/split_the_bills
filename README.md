# README

Services:

*  MailCatcher -> http://localhost:1080/ <br>
`command to run: mailcatcher`

*  Redis
`command to rub: redis-server`
`command to shutdown: redis-cli shutdown`

*  Sidekiq
`command to run: bundle exec sidekiq -e development -q mailers -q default`

Web-site pages:
*  Waiter page -> http://localhost:3000/waiter
*  Main page -> http://localhost:3000
*  Menu for waiters and admins -> http://localhost:3000/menu
