Rails.application.routes.draw do
  devise_for :users, controllers: {
    confirmations: 'confirmations'
  }
  root 'visitors#new'

  resource :sidekiq_job_report, only: :show

  resources :visitors
  resources :menus do
    collection do
      delete 'deleteDinner'
    end
  end
  resources :table do
    collection do
      patch 'confirmOrder'
      patch 'closeTable'
    end
  end
  resources :orders do
    collection do
      get 'getReceipt'
      post 'addOrder'
    end
  end

  devise_scope :user do
    get 'users/sign_out' => "devise/sessions#destroy"
  end

end
