class CreateVisitors < ActiveRecord::Migration[6.0]
  def change
    create_table :visitors do |t|
      t.string :nickname
      t.belongs_to :bill

      t.timestamps
    end
  end
end
